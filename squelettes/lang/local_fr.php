<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'pass_espace_prive_bla' => 'L\'espace priv&eacute; de ce site est bien &eacute;videmment ouvert aux internautes, apr&egrave;s inscription. Une fois enregistr&eacute;(e), vous pourrez participer &agrave; la vie du site en r&eacute;digeant des articles sur les sites &agrave;  &eacute;pingler dans l\'Herbier.',

'site_web' => 'site web',
'sites_web' => 'sites web',
'voir_archive_en_ligne' => 'Voir le site archiv&eacute;',

'ical_texte_rss_articles' => 'Le flux de syndication des articles de ce site se trouve &agrave; l\'adresse suivante :',

// Formulaire de login
'form_forum_identifiants' => 'Connectez-vous',
'login_login2' => 'Votre courriel&nbsp;:',
'login_login' => 'Courriel&nbsp;:',
'login_autre_identifiant' => 'Modifier',
'login_rester_identifie' => 'Rester identifi&eacute;e quelques jours',

//R
'resultat' => 'r&eacute;sultat',
'resultats' => 'r&eacute;sultats',
'resultat_aucun' => 'Aucun r&eacute;sultat pour cette recherche !',
'resultats_suivants' => 'Autres r&eacute;sultats',


'suite' => 'Lire la suite'

);

?>