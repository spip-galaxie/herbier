<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$type_urls = "arbo";

define ('_url_minuscules',1);

// Re-ecriture des URLs
$GLOBALS['url_arbo_types']=array(
'rubrique'=>'', // pas de type pour les rubriques
'article'=>'',
'mot'=>'',
'auteur'=>'contact',
'site'=>'liens',
);
$GLOBALS['url_arbo_parents']=array(
'rubrique'=>'',
'article'=>'',
'breve'=>'',
'site'=>'',
'mot'=>'');

// Limiter la taille des images uploadees
define('_LOGO_MAX_WIDTH',2000) ;
define('_LOGO_MAX_HEIGHT',9999) ;
define('_IMG_MAX_WIDTH',2000) ;
define('_IMG_MAX_HEIGHT',2000) ;
define('_IMG_MAX_SIZE',2048); /* 1Mo=1024Ko */
// define('_DOC_MAX_SIZE', 0);

?>
